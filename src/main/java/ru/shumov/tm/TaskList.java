package ru.shumov.tm;

import java.util.Scanner;

public class TaskList {

    static Scanner scrTaskList = new Scanner(System.in);

    public static void taskList() {
        if (CreateTask.tasks.isEmpty()) {
            System.out.println("Задач нет.");
        } else {
            System.out.println("Вывести все задачи?");
            System.out.println("yes/no");
            String answer = scrTaskList.nextLine();

            if (answer.equals("yes")) {
                for (int i = 0; CreateTask.tasks.size() > i; i++) {
                    System.out.println(CreateTask.tasks.get(i));
                }
            }
            if (answer.equals("no")) {
                System.out.println("Введите название проекта задачи которого нужно вывести:");
                String projectName = scrTaskList.nextLine();
                for (int i = 0; CreateTask.tasks.size() > i; i++) {
                    if (CreateTask.tasks.get(i).contains(projectName)) {
                        System.out.println(CreateTask.tasks.get(i));
                    }
                }
            }
        }
    }
}
