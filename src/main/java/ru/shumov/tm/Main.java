package ru.shumov.tm;

import java.util.Scanner;

import static ru.shumov.tm.Commands.*;
import static ru.shumov.tm.Help.help;


public class Main {

    public static Scanner scr = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("       Добро Пожаловать в Task Manager");
        System.out.println("...ID Проекта Можно Найти в Списке Проектов...");
        while (true) {

            String commandName = scr.nextLine();

            if (commandName.equals(Commands.HELP)) {
                Help.help();
            }
            if (commandName.equals(Commands.PROJECT_CREATE)) {
                System.out.println("Введите имя проекта:");
                String PrName = scr.nextLine();
                CreateProject.createProject(PrName);
            }
            if (commandName.equals(Commands.PROJECT_CLEAR)) {
                ProjectClear.projectClear();
            }
            if (commandName.equals(Commands.PROJECT_REMOVE)) {
                ProjectRemove.projectRemove();
            }
            if (commandName.equals(Commands.PROJECT_LIST)) {
                ProjectList.projectList();
            }
            if (commandName.equals(Commands.TASK_CREATE)) {
                CreateTask.createTask();
            }
            if (commandName.equals(Commands.TASK_CLEAR)) {
                TaskClear.taskClear();
            }
            if (commandName.equals(Commands.TASK_REMOVE)) {
                TaskRemove.taskRemove();
            }
            if (commandName.equals(Commands.TASK_LIST)) {
                TaskList.taskList();
            }
            if (commandName.equals(Commands.FINISH)) {
                break;
            }
        }
    }


}
