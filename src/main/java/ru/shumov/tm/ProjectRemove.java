package ru.shumov.tm;

import java.util.Scanner;

public class ProjectRemove {

    static Scanner scrProjectRe = new Scanner(System.in);

    public static void projectRemove() {
        System.out.println("При удалении проекта все задачи будут удалены.");
        System.out.println("Введите имя проекта:");
        String projectName = scrProjectRe.nextLine();
        for (int i = 0; CreateTask.tasks.size() > i; i++) {
            if (CreateTask.tasks.get(i).contains(projectName)) {
                CreateTask.tasks.remove(CreateTask.tasks.get(i));
            }
        }
        System.out.println("Введите ID Проекта который вы хотите удалить:");
        String projectId = scrProjectRe.nextLine();
        if (projectId != null) {
            CreateProject.projects.remove(projectId);
        }
    }
}
