package ru.shumov.tm;

import java.util.Scanner;

public class TaskClear {

    static Scanner scrTaskCl = new Scanner(System.in);

    public static void taskClear() {

        System.out.println("Все задачи будут удалены, Вы уверны?");
        System.out.println("yes/no");
        String answer = scrTaskCl.nextLine();

        if (answer.equals("yes")) {
            for (int i = 0; CreateTask.tasks.size() > i; i++) {
                CreateTask.tasks.removeAll(CreateTask.tasks);
            }
        }
        System.out.println("Done");
    }
}
