package ru.shumov.tm;

import java.util.Scanner;

public class TaskRemove {

    static Scanner scrTaskRe = new Scanner(System.in);

    public static void taskRemove() {

        System.out.println("Введите имя проекта и задачи которую вы хотите удалить через пробел:");
        String taskName = scrTaskRe.nextLine();

        for (int i = 0; CreateTask.tasks.size() > i; i++) {
            if (CreateTask.tasks.get(i).equals(taskName)) {
                CreateTask.tasks.remove(CreateTask.tasks.get(i));
            }
            System.out.println("Done");
        }
    }
}
