package ru.shumov.tm;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CreateProject {

    static Map<String, String> projects = new HashMap<>();

    public static void createProject(String name) {
        if (projects.containsValue(name)) {
            System.out.println("Такой проект уже существует");
        } else {
            String id = String.valueOf(UUID.randomUUID().toString());
            projects.put(id, name);
            System.out.println("Done");
        }
    }
}
