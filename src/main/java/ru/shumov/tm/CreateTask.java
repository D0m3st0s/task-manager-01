package ru.shumov.tm;


import java.util.ArrayList;
import java.util.Scanner;


public class CreateTask {

    static Scanner scrTasks = new Scanner(System.in);
    static ArrayList<String> tasks = new ArrayList<>();

    public static void createTask() {
        System.out.println("Чтобы добавить задание к проекту введите название проекта:");
        String projectName = scrTasks.nextLine();
        if (CreateProject.projects.containsValue(projectName)) {
            System.out.println("Введите назввание задачи:");
            String name = projectName + " " + scrTasks.nextLine();
            if (tasks.contains(name)) {
                System.out.println("Такая задача уже есть.");
            } else {
                tasks.add(name);
                System.out.println("Done");
            }
        } else {
            System.out.println("Такого проекта нет.");
        }
    }
}
