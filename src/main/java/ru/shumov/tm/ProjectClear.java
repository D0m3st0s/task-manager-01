package ru.shumov.tm;

import java.util.Scanner;

public class ProjectClear extends TaskClear {

    static Scanner scrPrClear = new Scanner(System.in);

    public static void projectClear() {

        System.out.println("Все проекты будут удалены, Вы уверны?");
        System.out.println("yes/no");
        String answer = scrPrClear.nextLine();

        if (answer.equals("yes")) {
            CreateProject.projects.clear();
            System.out.println("Done");

        }
        for (int i = 0; CreateTask.tasks.size() > i; i++) {
            CreateTask.tasks.removeAll(CreateTask.tasks);
        }
    }
}
